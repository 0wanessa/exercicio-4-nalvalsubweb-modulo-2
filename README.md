# grad API

## Setup
Install Python

## Set and activate virtual environment
In project folder, execute the following commands:

```bash
pip install pipenv
export PIPENV_VENV_IN_PROJECT="enabled"
mkdir .venv
pipenv shell
source .venv/Scripts/activate
```

## Install dependencies on virtual env
In project folder, execute the following command:

```bash
pipenv install --dev
```

## Run
```bash
python grad.py
```
